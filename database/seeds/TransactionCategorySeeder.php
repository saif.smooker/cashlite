<?php

use Illuminate\Database\Seeder;

class TransactionCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_categories')->insert([
			[
				'name'	 => 'Transfer',
				'type'	 => 'other',
				'system' => 1,
				'color'  => '#1abc9c',
			],[
				'name'	 => 'Deposit',
				'type'	 => 'income',
				'system' => 1,
				'color' => '#2ecc71'
			],[
				'name'	 => 'Sale',
				'type'	 => 'income',
				'system' => 1,
				'color' => '#4834d4'
			],[
				'name'	 => 'Other',
				'type'	 => 'expense',
				'system' => 1,
				'color' => '#eb4d4b'
			],
		]);
    }
}
