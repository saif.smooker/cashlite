<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->date('trans_date');
            $table->bigInteger('account_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();
			$table->string('type',10);
            $table->string('dr_cr',2);
            $table->decimal('amount',10,2);
            $table->decimal('currency_rate', 15, 8);
            $table->bigInteger('customer_id')->unsigned()->nullable();
            $table->bigInteger('vendor_id')->unsigned()->nullable();
            $table->bigInteger('payment_method_id')->unsigned();
            $table->string('reference')->nullable();
            $table->text('attachment')->nullable();
            $table->text('note')->nullable();
			$table->bigInteger('created_user_id')->nullable();
			$table->bigInteger('updated_user_id')->nullable();
            $table->timestamps();
			
			$table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
			$table->foreign('category_id')->references('id')->on('transaction_categories')->onDelete('cascade');
			$table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
