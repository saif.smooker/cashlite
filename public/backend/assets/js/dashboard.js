(function($) {
    "use strict";

	//Yearly Income
	var link = _url + "/dashboard/json_month_wise_income_expense/";
    $.ajax({
        url: link, 
		success: function (data) {
			var json = JSON.parse(data);
            var cashflow = echarts.init(document.getElementById('yearly_income_expense'));

            var option = {
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: [$lang_income, $lang_expense, $lang_profit]
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {show: true},
                        dataView: {
                            show: true,
                            readOnly: false,
                            title: 'Data View',
                            lang: ['Data View', 'Cancel', 'Reset']
                        },
                        magicType: {
                            show: true, title: {
                                line: 'Line',
                                bar: 'Bar',
                                tiled: 'Tiled',
                            }, type: ['line', 'bar', 'tiled']
                        },
                        restore: {show: true, title: 'Reset'},
                        saveAsImage: {
                            show: true, title: 'Save as Image',
                            type: 'png',
                            lang: ['Click to Save']
                        }
                    }

                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category',
                        boundaryGap: false,
                        data: json['Months']
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: $lang_income,
                        type: 'line',
                        color: [
                            '#1761fd'
                        ],
                        data: json['Income']
                    },
                    {
                        name: $lang_expense,
                        type: 'line',
                        color: [
                            '#fd3c97'
                        ],
                        data: json['Expense']
                    },
					{
                        name: $lang_profit,
                        type: 'line',
                        color: [
                            '#27ae60'
                        ],
                        data: json['Profit']
                    }
                ]
            };

            // use configuration item and data specified to show chart
            cashflow.setOption(option);

        }
    });
	
	//Income By Category Chart
    var link2 = _url + "/dashboard/json_income_by_category/";
    $.ajax({
        url: link2,
		success: function (data2) {
			var json2 = JSON.parse(data2);

            var income_by_category = echarts.init(document.getElementById('income_by_category'));
            var option2 = {
                tooltip: {
                    trigger: 'item',
					formatter: '{a} <br/>{b}: {c} ({d}%)'
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: json2['category']
                },
                series: [
                    {
						name: 'Income By Category',
                        type: 'pie',
                        radius: ['50%', '70%'],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true
                                },
                                labelLine: {
                                    show: true
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    position: 'center',
                                    textStyle: {
                                        fontSize: '24',
                                        fontWeight: 'bold'
                                    }
                                }
                            }
                        },
                        data: json2['data']
                    }
                ]
            };


            income_by_category.setOption(option2);
        }
    });
	
	//Expense By Category Chart
    var link2 = _url + "/dashboard/json_expense_by_category/";
    $.ajax({
        url: link2,
		success: function (data2) {
			var json2 = JSON.parse(data2);

            var income_by_category = echarts.init(document.getElementById('expense_by_category'));
            var option2 = {
                tooltip: {
                    trigger: 'item',
					formatter: '{a} <br/>{b}: {c} ({d}%)'
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: json2['category']
                },
                series: [
                    {
						name: 'Expense By Category',
                        type: 'pie',
                        radius: ['50%', '70%'],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true
                                },
                                labelLine: {
                                    show: true
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    position: 'center',
                                    textStyle: {
                                        fontSize: '24',
                                        fontWeight: 'bold'
                                    }
                                }
                            }
                        },
                        data: json2['data']
                    }
                ]
            };


            income_by_category.setOption(option2);
        }
    });
	

})(jQuery);	