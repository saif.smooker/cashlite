<li>
	<a href="javascript: void(0);"><i class="ti-face-smile"></i><span>{{ _lang('Customers') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li class="nav-item"><a class="nav-link" href="{{ route('customers.index') }}">{{ _lang('All Customers') }}</a></li>
		<li class="nav-item"><a class="nav-link" href="{{ route('customers.create') }}">{{ _lang('Add New') }}</a></li>		
	</ul>
</li>

<li>
	<a href="javascript: void(0);"><i class="ti-user"></i><span>{{ _lang('Vendors') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li class="nav-item"><a class="nav-link" href="{{ route('vendors.index') }}">{{ _lang('All Vendors') }}</a></li>
		<li class="nav-item"><a class="nav-link" href="{{ route('vendors.create') }}">{{ _lang('Add New') }}</a></li>		
	</ul>
</li>

<li><a href="{{ route('accounts.index') }}"><i class="ti-home"></i> <span>{{ _lang('Bank & Cash Account') }}</span></a></li>

<li>
	<a href="javascript: void(0);"><i class="ti-wallet"></i><span>{{ _lang('Transactions') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li class="nav-item"><a class="nav-link" href="{{ route('income.index') }}">{{ _lang('Manage Income') }}</a></li>	
		<li class="nav-item"><a class="nav-link" href="{{ route('expense.index') }}">{{ _lang('Manage Expense') }}</a></li>	
		<li class="nav-item"><a class="nav-link" href="{{ route('transfers.index') }}">{{ _lang('Transfer Money') }}</a></li>	
	</ul>
</li>

<li>
	<a href="javascript: void(0);"><i class="ti-user"></i><span>{{ _lang('User Management') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li class="nav-item"><a class="nav-link" href="{{ route('users.index') }}">{{ _lang('All Users') }}</a></li>
		<li class="nav-item"><a class="nav-link" href="{{ route('roles.index') }}">{{ _lang('User Roles') }}</a></li>		
		<li class="nav-item"><a class="nav-link" href="{{ route('permission.index') }}">{{ _lang('Access Control') }}</a></li>		
	</ul>
</li>

<li>
	<a href="javascript: void(0);"><i class="ti-bar-chart"></i><span>{{ _lang('Reports') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li class="nav-item"><a class="nav-link" href="{{ route('reports.income_summary') }}">{{ _lang('Income Summary') }}</a></li>
		<li class="nav-item"><a class="nav-link" href="{{ route('reports.expense_summary') }}">{{ _lang('Expense Summary') }}</a></li>	
		<li class="nav-item"><a class="nav-link" href="{{ route('reports.income_expense_summary') }}">{{ _lang('Income Vs Expense') }}</a></li>		
	</ul>
</li>

<li>
	<a href="javascript: void(0);"><i class="ti-world"></i><span>{{ _lang('Languages') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li class="nav-item"><a class="nav-link" href="{{ url('languages') }}">{{ _lang('All Language') }}</a></li>
		<li class="nav-item"><a class="nav-link" href="{{ url('languages/create') }}">{{ _lang('Add New') }}</a></li>		
	</ul>
</li>

<li>
	<a href="javascript: void(0);"><i class="ti-settings"></i><span>{{ _lang('Administration') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		<li class="nav-item"><a class="nav-link" href="{{ url('administration/general_settings') }}">{{ _lang('General Settings') }}</a></li>
		<li class="nav-item"><a class="nav-link" href="{{ route('currency.index') }}">{{ _lang('Currency Manager') }}</a></li>
		<li class="nav-item"><a class="nav-link" href="{{ route('transaction_categories.index') }}">{{ _lang('Transaction Category') }}</a></li>
		<li class="nav-item"><a class="nav-link" href="{{ route('payment_methods.index') }}">{{ _lang('Payment Methods') }}</a></li>
		<li class="nav-item"><a class="nav-link" href="{{ route('database_backups.list') }}">{{ _lang('Database Backup') }}</a></li>	
	</ul>
</li>