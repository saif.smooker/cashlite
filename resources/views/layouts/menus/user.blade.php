@php $permissions = permission_list(); @endphp

<li>
	<a href="javascript: void(0);"><i class="ti-face-smile"></i><span>{{ _lang('Customers') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		@if (in_array('customers.index',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('customers.index') }}">{{ _lang('All Customers') }}</a></li>
		@endif
		
		@if (in_array('customers.create',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('customers.create') }}">{{ _lang('Add New') }}</a></li>	
		@endif	
	</ul>
</li>

<li>
	<a href="javascript: void(0);"><i class="ti-user"></i><span>{{ _lang('Vendors') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		@if (in_array('vendors.index',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('vendors.index') }}">{{ _lang('All Vendors') }}</a></li>
		@endif

		@if (in_array('vendors.create',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('vendors.create') }}">{{ _lang('Add New') }}</a></li>
		@endif		
	</ul>
</li>

@if (in_array('accounts.index',$permissions))
	<li><a href="{{ route('accounts.index') }}"><i class="ti-home"></i> <span>{{ _lang('Bank & Cash Account') }}</span></a></li>
@endif	

<li>
	<a href="javascript: void(0);"><i class="ti-wallet"></i><span>{{ _lang('Transactions') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		@if (in_array('income.index',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('income.index') }}">{{ _lang('Manage Income') }}</a></li>	
		@endif

		@if (in_array('expense.index',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('expense.index') }}">{{ _lang('Manage Expense') }}</a></li>	
		@endif

		@if (in_array('transfers.index',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('transfers.index') }}">{{ _lang('Transfer Money') }}</a></li>
		@endif
	</ul>
</li>

<li>
	<a href="javascript: void(0);"><i class="ti-bar-chart"></i><span>{{ _lang('Reports') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		@if (in_array('reports.income_summary',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('reports.income_summary') }}">{{ _lang('Income Summary') }}</a></li>
		@endif

		@if (in_array('reports.expense_summary',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('reports.expense_summary') }}">{{ _lang('Expense Summary') }}</a></li>	
		@endif

		@if (in_array('reports.income_expense_summary',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('reports.income_expense_summary') }}">{{ _lang('Income Vs Expense') }}</a></li>	
		@endif	
	</ul>
</li>

<li>
	<a href="javascript: void(0);"><i class="ti-settings"></i><span>{{ _lang('Administration') }}</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
	<ul class="nav-second-level" aria-expanded="false">
		@if (in_array('currency.index',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('currency.index') }}">{{ _lang('Currency Manager') }}</a></li>
		@endif

		@if (in_array('transaction_categories.index',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('transaction_categories.index') }}">{{ _lang('Transaction Category') }}</a></li>
		@endif

		@if (in_array('payment_methods.index',$permissions))
			<li class="nav-item"><a class="nav-link" href="{{ route('payment_methods.index') }}">{{ _lang('Payment Methods') }}</a></li>
		@endif
	</ul>
</li>