@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card no-export">
		    <div class="card-header">
				<span class="panel-title">{{ _lang('Transaction Categories') }}</span>
				<button class="btn btn-primary btn-xs float-right ajax-modal" data-title="{{ _lang('Create New Category') }}" data-href="{{ route('transaction_categories.create') }}">{{ _lang('Add New') }}</button>
			</div>
			<div class="card-body">
				<table id="income_expense_categories_table" class="table table-bordered data-table">
					<thead>
					    <tr>
						    <th>{{ _lang('Name') }}</th>
							<th>{{ _lang('Type') }}</th>
							<th>{{ _lang('Color') }}</th>
							<th class="text-center">{{ _lang('Action') }}</th>
					    </tr>
					</thead>
					<tbody>
					    @foreach($categorys as $category)
					    <tr data-id="row_{{ $category->id }}">
							<td class='name'>{{ $category->name }}</td>
							<td class='type'>{{ ucwords($category->type) }}</td>
							<td class='color'>
								<div class="rounded-circle color-circle" style="background:{{ $category->color }}"></div>
							</td>
							
							<td class="text-center">
								<div class="dropdown">
								  <button class="btn btn-primary dropdown-toggle btn-xs" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								  {{ _lang('Action') }}
								  <i class="fas fa-angle-down"></i>
								  </button>

								  @if($category->system == 0)
									<form action="{{ action('CategoryController@destroy', $category['id']) }}" method="post">
										{{ csrf_field() }}
										<input name="_method" type="hidden" value="DELETE">
										
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<button data-href="{{ action('CategoryController@edit', $category['id']) }}" data-title="{{ _lang('Update Income/Expense Category') }}" class="dropdown-item dropdown-edit ajax-modal"><i class="mdi mdi-pencil"></i> {{ _lang('Edit') }}</button>
											<button class="btn-remove dropdown-item" type="submit"><i class="mdi mdi-delete"></i> {{ _lang('Delete') }}</button>
										</div>
									</form>
								  @else
								  	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<button data-href="{{ action('CategoryController@edit', $category['id']) }}" data-title="{{ _lang('Update Income/Expense Category') }}" class="dropdown-item dropdown-edit ajax-modal"><i class="mdi mdi-pencil"></i> {{ _lang('Edit') }}</button>
									</div>
								  @endif

								</div>
							</td>
					    </tr>
					    @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection