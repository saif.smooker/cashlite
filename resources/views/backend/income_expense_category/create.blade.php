@extends('layouts.app')

@section('content')
<link href="{{ asset('public/backend/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">

<div class="row">
	<div class="col-lg-6">
		<div class="card">
			<div class="card-header">
				<span class="panel-title">{{ _lang('Create New Category') }}</span>
			</div>
			<div class="card-body">
			    <form method="post" class="validate" autocomplete="off" action="{{ route('transaction_categories.store') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-12">
					        <div class="form-group">
						        <label class="control-label">{{ _lang('Name') }}</label>						
						        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
					        </div>
					    </div>

						<div class="col-md-12">
					        <div class="form-group">
						        <label class="control-label">{{ _lang('Type') }}</label>						
						        <select class="form-control auto-select" data-selected="{{ old('type') }}" name="type" required>
					                <option value="">{{ _lang('Select One') }}</option>
									<option value="income">{{ _lang('Income') }}</option>
									<option value="expense">{{ _lang('Expense') }}</option>
									<option value="other">{{ _lang('Other') }}</option>
								</select>
							</div>
					    </div>

					    <div class="col-md-12">
							<div class="form-group">
							   <label class="control-label">{{ _lang('Color') }}</label>						
							   <input type="text" class="form-control colorpicker" name="color" value="{{ old('color') }}" required>
							</div>
						</div>

							
						<div class="col-md-12">
							<div class="form-group">
								<button type="submit" class="btn btn-primary">{{ _lang('Save') }}</button>
							</div>
						</div>
					</div>			
			    </form>
			</div>
		</div>
    </div>
</div>
@endsection

@section('js-script')
<script src="{{ asset('public/backend/plugins/bootstrap-colorpicker/bootstrap-colorpicker.js') }}"></script>
@endsection

