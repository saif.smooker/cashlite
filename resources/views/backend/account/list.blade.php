@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card no-export">
		    <div class="card-header">
				<span class="panel-title">{{ _lang('Account List') }}</span>
				<button class="btn btn-primary btn-xs float-right ajax-modal" data-title="{{ _lang('Add Account') }}" data-href="{{ route('accounts.create') }}">{{ _lang('Add New') }}</button>
			</div>
			<div class="card-body">
				<table id="accounts_table" class="table table-bordered data-table">
					<thead>
					    <tr>
						    <th>{{ _lang('Name') }}</th>
							<th>{{ _lang('Account No') }}</th>
							<th>{{ _lang('Currency') }}</th>
							<th>{{ _lang('Openning Balance') }}</th>
							<th>{{ _lang('Created At') }}</th>
							<th class="text-center">{{ _lang('Action') }}</th>
					    </tr>
					</thead>
					<tbody>
					    @foreach($accounts as $account)
					    <tr data-id="row_{{ $account->id }}">
							<td class='name'>{{ $account->name }}</td>
							<td class='account_no'>{{ $account->account_no }}</td>
							<td class='currency_id'>{{ $account->currency->name }}</td>
							<td class='openning_balance'>{!! xss_clean(decimalPlace($account->openning_balance, $account->currency->name)) !!}</td>
							<td class='created_at'>{{ $account->created_at }}</td>
							<td class="text-center">
								<div class="dropdown">
								  <button class="btn btn-primary dropdown-toggle btn-xs" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								  {{ _lang('Action') }}
								  <i class="fas fa-angle-down"></i>
								  </button>
								  <form action="{{ action('AccountController@destroy', $account['id']) }}" method="post">
									{{ csrf_field() }}
									<input name="_method" type="hidden" value="DELETE">
									
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<button data-href="{{ action('AccountController@edit', $account['id']) }}" data-title="{{ _lang('Update Account') }}" class="dropdown-item dropdown-edit ajax-modal"><i class="mdi mdi-pencil"></i> {{ _lang('Edit') }}</button>
										<button data-href="{{ action('AccountController@show', $account['id']) }}" data-title="{{ _lang('View Account') }}" class="dropdown-item dropdown-view ajax-modal"><i class="mdi mdi-eye"></i> {{ _lang('View') }}</button>
										<button class="btn-remove dropdown-item" type="submit"><i class="mdi mdi-delete"></i> {{ _lang('Delete') }}</button>
									</div>
								  </form>
								</div>
							</td>
					    </tr>
					    @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection