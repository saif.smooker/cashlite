@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card no-export">
		    <div class="card-header">
				<span class="panel-title">{{ _lang('Transfers') }}</span>
				<a class="btn btn-primary btn-xs float-right" href="{{ route('transfers.create') }}">{{ _lang('Add New Transfer') }}</a>
			</div>
			<div class="card-body">
				<table id="transfers_table" class="table table-bordered data-table">
					<thead>
					    <tr>
						    <th>{{ _lang('Date') }}</th>
							<th>{{ _lang('From Account') }}</th>
							<th>{{ _lang('To Account') }}</th>
							<th class="text-right">{{ _lang('Amount') }}</th>
							<th class="text-center">{{ _lang('Action') }}</th>
					    </tr>
					</thead>
					<tbody>
					    @foreach($transfers as $transfer)
					    <tr data-id="row_{{ $transfer->id }}">
							<td class='trans_date'>{{ $transfer->expense->paid_at }}</td>
							<td class='from_account'>{{ $transfer->expense->account->name }}</td>
							<td class='to_account'>{{ $transfer->income->account->name }}</td>
							<td class='amount text-right'>
								{!! xss_clean(decimalPlace($transfer->expense->amount, $transfer->expense->account->currency->name)) !!}
							</td>
							
							<td class="text-center">
								<div class="dropdown">
								  <button class="btn btn-primary dropdown-toggle btn-xs" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								  {{ _lang('Action') }}
								  <i class="fas fa-angle-down"></i>
								  </button>
								  <form action="{{ action('TransferController@destroy', $transfer['id']) }}" method="post">
									{{ csrf_field() }}
									<input name="_method" type="hidden" value="DELETE">
									
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a href="{{ action('TransferController@edit', $transfer['id']) }}" class="dropdown-item dropdown-edit dropdown-edit"><i class="mdi mdi-pencil"></i> {{ _lang('Edit') }}</a>
										<button class="btn-remove dropdown-item" type="submit"><i class="mdi mdi-delete"></i> {{ _lang('Delete') }}</button>
									</div>
								  </form>
								</div>
							</td>
					    </tr>
					    @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection