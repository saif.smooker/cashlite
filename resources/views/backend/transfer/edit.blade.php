@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<span class="panel-title">{{ _lang('Update Transfer') }}</span>
			</div>
			<div class="card-body">
				<form method="post" class="validate" autocomplete="off" action="{{ action('TransferController@update', $id) }}" enctype="multipart/form-data">
					{{ csrf_field()}}
					<input name="_method" type="hidden" value="PATCH">				
					<div class="row">
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('From Account') }}</label>						
							<select class="form-control select2 auto-select" data-selected="{{ $transfer->expense->account_id }}" name="from_account" id="from_account" required>
							   <option value="">{{ _lang('Select One') }}</option>
							   @foreach(\App\Account::all() as $account)
									<option value="{{ $account->id }}">{{ $account->name.' - '.$account->currency->name }}</option>
							   @endforeach
							</select>
						  </div>
						</div>

						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('To Account') }}</label>						
							<select class="form-control select2 auto-select" data-selected="{{ $transfer->income->account_id }}" name="to_account" id="to_account" required>
							   <option value="">{{ _lang('Select One') }}</option>
							   @foreach(\App\Account::all() as $account)
									<option value="{{ $account->id }}">{{ $account->name.' - '.$account->currency->name }}</option>
							   @endforeach
							</select>
						  </div>
						</div>


						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Date') }}</label>						
							<input type="text" class="form-control datepicker" name="trans_date" value="{{ $transfer->expense->trans_date }}" required>
						  </div>
						</div>

						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Amount') }}</label>						
							<input type="text" class="form-control float-field" name="amount" value="{{ $transfer->expense->amount }}" required>
						  </div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">{{ _lang('Payment Method') }}</label>						
								<select class="form-control select2" name="payment_method_id" required>
								   <option value="">{{ _lang('Select One') }}</option>
								   {{ create_option("payment_methods","id","name",$transfer->expense->payment_method_id) }}
								</select>
							</div>
						</div>

						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Reference') }}</label>						
							<input type="text" class="form-control" name="reference" value="{{ $transfer->expense->reference }}">
						  </div>
						</div>

						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Attachment') }}</label>						
							<input type="file" class="form-control trickycode-file" name="attachment" data-value="{{ $transfer->expense->attachment }}">
						  </div>
						</div>

						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Note') }}</label>						
							<textarea class="form-control" name="note">{{ $transfer->expense->note }}</textarea>
						  </div>
						</div>

							
						<div class="col-md-12">
							<div class="form-group">
								<button type="submit" class="btn btn-primary">{{ _lang('Update') }}</button>
							</div>
						</div>	
					</div>	
				</form>
			</div>
		</div>
	</div>
</div>

@endsection


