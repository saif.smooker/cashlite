@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
		    <div class="card-header">
				<span class="panel-title">{{ _lang('View Transfer') }}</span>
			</div>
			
			<div class="card-body">
			    <table class="table table-bordered">
				    <tr><td>{{ _lang('Expense Transaction Id') }}</td><td>{{ $transfer->expense_transaction_id }}</td></tr>
					<tr><td>{{ _lang('Income Transaction Id') }}</td><td>{{ $transfer->income_transaction_id }}</td></tr>
			    </table>
			</div>
	    </div>
	</div>
</div>
@endsection


