@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-lg-12">
	    <div class="alert alert-warning">
			<strong>{{ _lang('Amounts are Showing in base currency').' ('.currency().')' }}</strong>
		</div>
		
		<div class="card">
			<div class="card-header">
				<span class="panel-title">{{ _lang('Income Vs Expense Summary') }}</span>
				
				<form method="POST" action="{{ route('reports.income_expense_summary') }}" class="float-right">
					{{ csrf_field() }}
        			<div id="items" class="float-left">
                        <select class="form-control-sm d-inline-block w-auto auto-select" data-selected="{{ isset($year) ? $year : date('Y') }}" name="year">
                        	<option value="2023">2023</option>
                        	<option value="2022">2022</option>
                        	<option value="2021">2021</option>
                        	<option value="2020">2020</option>
                        	<option value="2019">2019</option>
                        </select>

                        <select class="form-control-sm d-inline-block w-auto auto-select" data-selected="{{ isset($account) ? $account : '' }}" name="account">
                        	<option value="">{{ _lang('Select Account') }}</option>
                        	@foreach(\App\Account::all() as $account)
								<option value="{{ $account->id }}">{{ $account->name.' - '.$account->currency->name }}</option>
							@endforeach
                        </select>
                        
                        <select class="form-control-sm d-inline-block w-auto auto-select" data-selected="{{ isset($customer) ? $customer : '' }}" name="customer">	
                        	<option value="">{{ _lang('Select Customer') }}</option>
                        	{{ create_option("customers", "id", "name", old('customer_id')) }}
                        </select>

                        <select class="form-control-sm d-inline-block w-auto auto-select" data-selected="{{ isset($vendor) ? $vendor : '' }}" name="cuvendorstomer">    
                            <option value="">{{ _lang('Select Vendor') }}</option>
                            {{ create_option("vendors", "id", "name") }}
                        </select>

                        <select class="form-control-sm d-inline-block w-auto auto-select"  data-selected="{{ isset($category) ? $category : '' }}" name="category">
                        	<option value="">{{ _lang('Select Category') }}</option>
                        	{{ create_option("transaction_categories","id","name",'', array('type != ' => 'other')) }}
                        </select>

                        <button type="submit" class="btn btn-xs btn-light"><span class="fa fa-filter"></span> &nbsp;Filter</button>
        			</div>

    			</form>

			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<div id="income_expense_summary"></div>
					</div>
                </div>				
			</div>
		</div>
	</div>
</div>
@endsection

@section('js-script')
<script src="{{ asset('public/backend/plugins/echart/echarts.min.js') }}"></script>

<script>
(function($) {
    "use strict";

    var income_expense_summary = echarts.init(document.getElementById('income_expense_summary'));

    var option = {
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['{{ _lang('Income') }}','{{ _lang('Expense') }}']
        },
        toolbox: {
            show: true,
            feature: {
                mark: {show: true},
                dataView: {
                    show: true,
                    readOnly: false,
                    title: 'Data View',
                    lang: ['Data View', 'Cancel', 'Reset']
                },
                magicType: {
                    show: true, title: {
                        line: 'Line',
                        bar: 'Bar',
                    }, type: ['line', 'bar']
                },
                restore: {show: true, title: 'Reset'},
                saveAsImage: {
                    show: true, title: 'Save as Image',
                    type: 'png',
                    lang: ['Click to Save']
                }
            }

        },
        calculable: true,
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                data: [{!! isset($Months) ? $Months : '' !!}]
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: '{{ _lang('Income') }}',
                type: 'line',
                color: [
                    '#1761fd'
                ],
                data: [{{ isset($Income) ? $Income : '' }}]
            },
            {
                name: '{{ _lang('Expense') }}',
                type: 'line',
                color: [
                    '#e74c3c'
                ],
                data: [{{ isset($Expense) ? $Expense : '' }}]
            }
        ]
    };

    // use configuration item and data specified to show chart
    income_expense_summary.setOption(option);

})(jQuery);	 

</script>   
@endsection

