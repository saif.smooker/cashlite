@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-4 mb-3">
		<div class="card">
			<div class="seo-fact sbg1">
				<div class="p-4">
					<div class="seofct-icon">
						<span>{{ _lang('Total Income') }}</span>
					</div>
					<h2>{!! xss_clean(decimalPlace($total_income,currency())) !!}</h2>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="col-md-4 mb-3">
		<div class="card">
			<div class="seo-fact sbg3">
				<div class="p-4">
					<div class="seofct-icon">
						<span>{{ _lang('Total Expense') }}</span>
					</div>
					<h2>{!! xss_clean(decimalPlace($total_expense,currency())) !!}</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-4 mb-3">
		<div class="card">
			<div class="seo-fact sbg2">
				<div class="p-4">
					<div class="seofct-icon">
						<span>{{ _lang('Total Profit') }}</span>
					</div>
					<h2>{!! xss_clean(decimalPlace(($total_income-$total_expense),currency())) !!}</h2>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row d-flex align-items-stretch">
	<!-- Cashflow -->
	<div class="col-lg-12 mt-2">
		<div class="card h-100">
			<div class="card-body">
				<h4 class="header-title">{{ _lang('Cash Flow').' '.date('Y') }}</h4>
				<div id="yearly_income_expense"></div>
			</div>
		</div>
	</div>
	<!-- End Cashflow -->
</div>

<div class="row">	
	<!-- Income By category -->
	<div class="col-lg-6 mt-5">
		<div class="card">
			<div class="card-body">
				<h4 class="header-title">{{ _lang('Income By Category') }}</h4>
				<div id="income_by_category"></div>
			</div>
		</div>
	</div>
	<!-- Income By category End -->

	<!-- Expense By category -->
	<div class="col-lg-6 mt-5">
		<div class="card">
			<div class="card-body">
				<h4 class="header-title">{{ _lang('Expenses By Category') }}</h4>
				<div id="expense_by_category"></div>
			</div>
		</div>
	</div>
	<!-- Expense By category End -->
</div>

<div class="row">	
	<!-- Account Balance -->
	<div class="col-lg-4 mt-5">
		<div class="card">
			<div class="card-body">
				<h4 class="header-title">{{ _lang('Account Balance') }}</h4>
				<table class="table table-bordered">
					<thead>
						<th>{{ _lang('Name') }}</th>
						<th class="text-right">{{ _lang('Balance') }}</th>
					</thead>
					<tbody>
					@foreach(get_financial_balance() as $account)
						<tr>
							<td>{{ $account->name }}</td>
							<td class="text-right">{!! xss_clean(decimalPlace($account->balance,$account->currency)) !!}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- Account Balance End -->

	<!-- Latest Income category -->
	<div class="col-lg-4 mt-5">
		<div class="card">
			<div class="card-body">
				<h4 class="header-title">{{ _lang('Latest Income') }}</h4>
				<table class="table table-bordered">
					<thead>
						<th>{{ _lang('Date') }}</th>
						<th>{{ _lang('Category') }}</th>
						<th class="text-right">{{ _lang('Amount') }}</th>
					</thead>
					<tbody>
					@foreach($recent_income as $income)
						<tr>
							<td>{{ $income->paid_at }}</td>
							<td>{{ $income->income_type->name }}</td>
							<td class="text-right">
								{!! xss_clean(decimalPlace($income->amount,$income->account->currency->name)) !!}
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- Latest Income End -->

	<!-- Latest Expenses -->
	<div class="col-lg-4 mt-5">
		<div class="card">
			<div class="card-body">
				<h4 class="header-title">{{ _lang('Latest Expenses') }}</h4>
				<table class="table table-bordered">
					<thead>
						<th>{{ _lang('Date') }}</th>
						<th>{{ _lang('Category') }}</th>
						<th class="text-right">{{ _lang('Amount') }}</th>
					</thead>
					<tbody>
					@foreach($recent_expense as $expense)
						<tr>
							<td>{{ $expense->paid_at }}</td>
							<td>{{ $expense->expense_type->name }}</td>
							<td class="text-right">
								{!! xss_clean(decimalPlace($expense->amount,$expense->account->currency->name)) !!}
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- Latest Expenses End -->
</div>

@endsection
        