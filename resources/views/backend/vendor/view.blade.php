@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
		    <div class="card-header">
				<span class="panel-title">{{ _lang('View Vendor') }}</span>
			</div>
			
			<div class="card-body">
			    <table class="table table-bordered">
				    <tr><td>{{ _lang('Name') }}</td><td>{{ $vendor->name }}</td></tr>
					<tr><td>{{ _lang('Company Name') }}</td><td>{{ $vendor->company_name }}</td></tr>
					<tr><td>{{ _lang('Email') }}</td><td>{{ $vendor->email }}</td></tr>
					<tr><td>{{ _lang('Registration No') }}</td><td>{{ $vendor->registration_no }}</td></tr>
					<tr><td>{{ _lang('Vat ID') }}</td><td>{{ $vendor->vat_id }}</td></tr>
					<tr><td>{{ _lang('Phone') }}</td><td>{{ $vendor->phone }}</td></tr>
					<tr><td>{{ _lang('Country') }}</td><td>{{ $vendor->country }}</td></tr>
					<tr><td>{{ _lang('City') }}</td><td>{{ $vendor->city }}</td></tr>
					<tr><td>{{ _lang('State') }}</td><td>{{ $vendor->state }}</td></tr>
					<tr><td>{{ _lang('Zip') }}</td><td>{{ $vendor->zip }}</td></tr>
					<tr><td>{{ _lang('Address') }}</td><td>{{ $vendor->address }}</td></tr>
					<tr><td>{{ _lang('Note') }}</td><td>{{ $vendor->note }}</td></tr>
			    </table>
			</div>
	    </div>
	</div>
</div>
@endsection


