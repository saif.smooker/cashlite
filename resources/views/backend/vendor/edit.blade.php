@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<span class="panel-title">{{ _lang('Update Vendor') }}</span>
			</div>
			<div class="card-body">
				<form method="post" class="validate" autocomplete="off" action="{{ action('VendorController@update', $id) }}" enctype="multipart/form-data">
					{{ csrf_field()}}
					<input name="_method" type="hidden" value="PATCH">				
					<div class="row">
						<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Name') }}</label>						
								   <input type="text" class="form-control" name="name" value="{{ $vendor->name }}" required>
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Company Name') }}</label>						
								   <input type="text" class="form-control" name="company_name" value="{{ $vendor->company_name }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Email') }}</label>						
								   <input type="text" class="form-control" name="email" value="{{ $vendor->email }}" required>
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Registration No') }}</label>						
								   <input type="text" class="form-control" name="registration_no" value="{{ $vendor->registration_no }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Vat ID') }}</label>						
								   <input type="text" class="form-control" name="vat_id" value="{{ $vendor->vat_id }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Phone') }}</label>						
								   <input type="text" class="form-control" name="phone" value="{{ $vendor->phone }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								    <label class="control-label">{{ _lang('Country') }}</label>						
								    <select class="form-control auto-select select2" data-selected="{{ $vendor->country }}" name="country">
						                <option value="">{{ _lang('Select One') }}</option>
										{{ get_country_list($vendor->country) }}
								    </select>
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('City') }}</label>						
								   <input type="text" class="form-control" name="city" value="{{ $vendor->city }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('State') }}</label>						
								   <input type="text" class="form-control" name="state" value="{{ $vendor->state }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Zip') }}</label>						
								   <input type="text" class="form-control" name="zip" value="{{ $vendor->zip }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Address') }}</label>						
								   <textarea class="form-control" name="address">{{ $vendor->address }}</textarea>
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Note') }}</label>						
								   <textarea class="form-control" name="note">{{ $vendor->note }}</textarea>
							    </div>
							</div>

							
						<div class="col-md-12">
							<div class="form-group">
								<button type="submit" class="btn btn-primary">{{ _lang('Update') }}</button>
							</div>
						</div>	
					</div>	
				</form>
			</div>
		</div>
	</div>
</div>

@endsection


