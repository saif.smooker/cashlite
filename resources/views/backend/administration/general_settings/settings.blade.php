@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-3">
			<ul class="nav flex-column nav-tabs settings-tab" role="tablist">
				 <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#general"><i class="ti-settings"></i> {{ _lang('General Settings') }}</a></li>
				  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#currency_settings"><i class="ti-money"></i> {{ _lang('Currency Settings') }}</a></li>
				 <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#email"><i class="ti-email"></i> {{ _lang('Email Settings') }}</a></li>
				 <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#logo"><i class="ti-image"></i> {{ _lang('Logo and Favicon') }}</a></li>
				 <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#cache"><i class="ti-server"></i> {{ _lang('Cache Control') }}</a></li>
			</ul>
		</div>
		  
		@php $settings = \App\Setting::all(); @endphp
		  
		<div class="col-sm-9">
			<div class="tab-content">
				<div id="general" class="tab-pane active">
					<div class="card">

						<div class="card-header">
							<span class="panel-title">{{ _lang('General Settings') }}</span>
						</div>

						<div class="card-body">
							 <form method="post" class="settings-submit params-panel" autocomplete="off" action="{{ route('settings.update_settings','store') }}" enctype="multipart/form-data">
								{{ csrf_field() }}
								<div class="row">
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('Company Name') }}</label>				
										<input type="text" class="form-control" name="company_name" value="{{ get_setting($settings, 'company_name') }}" required>
									  </div>
									</div>
									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('Site Title') }}</label>						
										<input type="text" class="form-control" name="site_title" value="{{ get_setting($settings, 'site_title') }}" required>
									  </div>
									</div>
									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('Phone') }}</label>						
										<input type="text" class="form-control" name="phone" value="{{ get_setting($settings, 'phone') }}">
									  </div>
									</div>
									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('Email') }}</label>						
										<input type="email" class="form-control" name="email" value="{{ get_setting($settings, 'email') }}">
									  </div>
									</div>

									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('Timezone') }}</label>						
										<select class="form-control select2" name="timezone" required>
										<option value="">{{ _lang('-- Select One --') }}</option>
										{{ create_timezone_option(get_setting($settings, 'timezone')) }}
										</select>
									  </div>
									</div>
									
													
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('Language') }}</label>						
										<select class="form-control select2" name="language">
											<option value="">{{ _lang('-- Select One --') }}</option>
											{{ load_language( get_setting($settings, 'language') ) }}
										</select>
									  </div>
									</div>

									 <div class="col-md-6">
									  	<div class="form-group">
											<label class="control-label">{{ _lang('Backend Direction') }}</label>						
											<select class="form-control auto-select" data-selected="{{ get_setting($settings, 'backend_direction','ltr') }}" name="backend_direction" required>
												<option value="ltr">{{ _lang('LTR') }}</option>
												<option value="rtl">{{ _lang('RTL') }}</option>
											</select>
									  	</div>
									</div>
								
									
									<div class="col-md-6">
									  	<div class="form-group">
											<label class="control-label">{{ _lang('Date Format') }}</label>						
											<select class="form-control auto-select" name="date_format" data-selected="{{ get_setting($settings, 'date_format','Y-m-d') }}" required>
												<option value="Y-m-d">{{ date('Y-m-d') }}</option>
												<option value="d-m-Y">{{ date('d-m-Y') }}</option>
												<option value="d/m/Y">{{ date('d/m/Y') }}</option>
												<option value="m-d-Y">{{ date('m-d-Y') }}</option>
												<option value="m.d.Y">{{ date('m.d.Y') }}</option>
												<option value="m/d/Y">{{ date('m/d/Y') }}</option>
												<option value="d.m.Y">{{ date('d.m.Y') }}</option>
												<option value="d/M/Y">{{ date('d/M/Y') }}</option>
												<option value="d/M/Y">{{ date('M/d/Y') }}</option>
												<option value="d M, Y">{{ date('d M, Y') }}</option>
											</select>
									  	</div>
									</div>
									
									<div class="col-md-6">
									  	<div class="form-group">
											<label class="control-label">{{ _lang('Time Format') }}</label>						
											<select class="form-control auto-select" name="time_format" data-selected="{{ get_setting($settings, 'time_format',24) }}" required>
												<option value="24">{{ _lang('24 Hours') }}</option>
												<option value="12">{{ _lang('12 Hours') }}</option>
											</select>
									  	</div>
									</div>
										
									<div class="col-md-12">
									  <div class="form-group">
										<button type="submit" class="btn btn-primary">{{ _lang('Save Settings') }}</button>
									  </div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>


				<div id="currency_settings" class="tab-pane fade">
					<div class="card">
						<div class="card-header">
							<span class="panel-title">{{ _lang('Currency Settings') }}</span>
						</div>

						<div class="card-body"> 
						   <form method="post" class="settings-submit params-panel" autocomplete="off" action="{{ route('settings.update_settings','store') }}" enctype="multipart/form-data">
								{{ csrf_field() }}
								<div class="row">
									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('Currency Position') }}</label>						
										<select class="form-control auto-select" data-selected="{{ get_setting($settings, 'currency_position','left') }}" name="currency_position" required>
											<option value="left">{{ _lang('Left') }}</option>
											<option value="right">{{ _lang('Right') }}</option>
										</select>
									  </div>
									</div>


									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('Thousand Seperator') }}</label>	
										<input type="text" class="form-control" name="thousand_sep" value="{{ get_setting($settings, 'thousand_sep',',') }}">
									  </div>
									</div>

									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('Decimal Seperator') }}</label>	
										<input type="text" class="form-control" name="decimal_sep" value="{{ get_setting($settings, 'decimal_sep','.') }}">
									  </div>
									</div>

									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('Decimal Places') }}</label>	
										<input type="text" class="form-control" name="decimal_places" value="{{ get_setting($settings, 'decimal_places',2) }}">
									  </div>
									</div>

									<div class="col-md-12">
									  <div class="form-group">
										<button type="submit" class="btn btn-primary">{{ _lang('Save Settings') }}</button>
									  </div>
									</div>	
								</div>							
							</form>
						</div>
					</div>
				</div>
				 
				
				<div id="email" class="tab-pane fade">
					<div class="card">
						<div class="card-header">
							<span class="panel-title">{{ _lang('Email Settings') }}</span>
						</div>

					    <div class="card-body">
							<form method="post" class="settings-submit params-panel" autocomplete="off" action="{{ route('settings.update_settings','store') }}" enctype="multipart/form-data">
								{{ csrf_field() }}
								<div class="row">
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('Mail Type') }}</label>						
										<select class="form-control auto-select" data-selected="{{ get_setting($settings, 'mail_type','mail') }}" name="mail_type" id="mail_type" required>
										  <option value="mail">{{ _lang('PHP Mail') }}</option>
										  <option value="smtp">{{ _lang('SMTP') }}</option>
										  <option value="sendmail">{{ _lang('Sendmail') }}</option>
										</select>
									  </div>
									</div>
									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('From Email') }}</label>						
										<input type="text" class="form-control" name="from_email" value="{{ get_setting($settings, 'from_email') }}" required>
									  </div>
									</div>
									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('From Name') }}</label>						
										<input type="text" class="form-control" name="from_name" value="{{ get_setting($settings, 'from_name') }}" required>
									  </div>
									</div>
									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('SMTP Host') }}</label>						
										<input type="text" class="form-control smtp" name="smtp_host" value="{{ get_setting($settings, 'smtp_host') }}">
									  </div>
									</div>
									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('SMTP Port') }}</label>						
										<input type="text" class="form-control smtp" name="smtp_port" value="{{ get_setting($settings, 'smtp_port') }}">
									  </div>
									</div>
									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('SMTP Username') }}</label>						
										<input type="text" class="form-control smtp" autocomplete="off" name="smtp_username" value="{{ get_setting($settings, 'smtp_username') }}">
									  </div>
									</div>
									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('SMTP Password') }}</label>						
										<input type="password" class="form-control smtp" autocomplete="off" name="smtp_password" value="{{ get_setting($settings, 'smtp_password') }}">
									  </div>
									</div>
									
									<div class="col-md-6">
									  <div class="form-group">
										<label class="control-label">{{ _lang('SMTP Encryption') }}</label>						
										<select class="form-control smtp auto-select" data-selected="{{ get_setting($settings, 'smtp_encryption','ssl') }}" name="smtp_encryption">
										   <option value="">{{ _lang('None') }}</option>
										   <option value="ssl">{{ _lang('SSL') }}</option>
										   <option value="tls">{{ _lang('TLS') }}</option>
										</select>
									  </div>
									</div>
									
									<div class="col-md-12">
									  <div class="form-group">
										<button type="submit" class="btn btn-primary">{{ _lang('Save Settings') }}</button>
									  </div>
									</div>
								</div>						
							</form>
					    </div>
					</div>
				</div>
							  				  
				<div id="logo" class="tab-pane fade">
					<div class="card">
						<div class="card-header">
							<span class="panel-title">{{ _lang('Logo and Favicon') }}</span>
						</div>

						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<form method="post" class="settings-submit params-panel" autocomplete="off" action="{{ route('settings.uplaod_logo') }}" enctype="multipart/form-data">				         	
										{{ csrf_field() }}
										<div class="row">
											<div class="col-md-12">
											  <div class="form-group">
												<label class="control-label">{{ _lang('Upload Logo') }}</label>						
												<input type="file" class="form-control dropify" name="logo" data-max-file-size="8M" data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" data-default-file="{{ get_logo() }}" required>
											  </div>
											</div>
											
											<br>
											<div class="col-md-12">
											  <div class="form-group">
												<button type="submit" class="btn btn-primary btn-block">{{ _lang('Upload') }}</button>
											  </div>
											</div>	
										</div>	
									</form>
								</div>

								<div class="col-md-6">
									<form method="post" class="settings-submit params-panel" autocomplete="off" action="{{ route('settings.update_settings','store') }}" enctype="multipart/form-data">	
										{{ csrf_field() }}
										<div class="row">	
											<div class="col-md-12">
											  <div class="form-group">
												<label class="control-label">{{ _lang('Upload Favicon') }} (PNG)</label>						
												<input type="file" class="form-control dropify" name="favicon" data-max-file-size="2M" data-allowed-file-extensions="png" data-default-file="{{ get_favicon() }}" required>
											  </div>
											</div>
											
											<br>
											<div class="col-md-12">
											  <div class="form-group">
												<button type="submit" class="btn btn-primary btn-block">{{ _lang('Upload') }}</button>
											  </div>
											</div>	
										</div>
                                    </form>										
								</div>									
							</div>
				  		</div>
			   		</div>  
				</div><!--End Logo Tab-->


				<div id="cache" class="tab-pane fade">
					<div class="card">
						<div class="card-header">
							<span class="panel-title">{{ _lang('Cache Control') }}</span>
						</div>

						<div class="card-body">
							<form method="post" class="params-panel" autocomplete="off" action="{{ route('settings.remove_cache') }}">	
								{{ csrf_field() }}
								<div class="row">	
									<div class="col-md-12">
									  	<div class="checkbox">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" name="cache[view_cache]" value="view_cache" id="view_cache">
												<label class="custom-control-label" for="view_cache">{{ _lang('View Cache') }}</label>
											</div>
										</div>
									</div>

									<div class="col-md-12">
									  	<div class="checkbox">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" name="cache[application_cache]" value="application_cache" id="application_cache">
												<label class="custom-control-label" for="application_cache">{{ _lang('Application Cache') }}</label>
											</div>
										</div>
									</div>
									
									<br>
									<br>
									<div class="col-md-12">
									  <div class="form-group">
										<button type="submit" class="btn btn-primary">{{ _lang('Remove Cache') }}</button>
									  </div>
									</div>	
								</div>
                            </form>										
				  		</div>
			   		</div>  
				</div><!--End Cache Tab-->
		</div>
	</div>
</div>
@endsection
