@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<span class="panel-title">{{ _lang('Update Customer') }}</span>
			</div>
			<div class="card-body">
				<form method="post" class="validate" autocomplete="off" action="{{ action('CustomerController@update', $id) }}" enctype="multipart/form-data">
					{{ csrf_field()}}
					<input name="_method" type="hidden" value="PATCH">				
					<div class="row">
						<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Name') }}</label>						
								   <input type="text" class="form-control" name="name" value="{{ $customer->name }}" required>
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Company Name') }}</label>						
								   <input type="text" class="form-control" name="company_name" value="{{ $customer->company_name }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Email') }}</label>						
								   <input type="text" class="form-control" name="email" value="{{ $customer->email }}" required>
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Phone') }}</label>						
								   <input type="text" class="form-control" name="phone" value="{{ $customer->phone }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								    <label class="control-label">{{ _lang('Country') }}</label>						
								    <select class="form-control auto-select select2" data-selected="{{ $customer->country }}" name="country">
						                <option value="">{{ _lang('Select One') }}</option>
										{{ get_country_list($customer->country) }}
								    </select>
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('City') }}</label>						
								   <input type="text" class="form-control" name="city" value="{{ $customer->city }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('State') }}</label>						
								   <input type="text" class="form-control" name="state" value="{{ $customer->state }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Zip') }}</label>						
								   <input type="text" class="form-control" name="zip" value="{{ $customer->zip }}">
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Address') }}</label>						
								   <textarea class="form-control" name="address">{{ $customer->address }}</textarea>
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="form-group">
								   <label class="control-label">{{ _lang('Note') }}</label>						
								   <textarea class="form-control" name="note">{{ $customer->note }}</textarea>
							    </div>
							</div>

							
						<div class="col-md-12">
							<div class="form-group">
								<button type="submit" class="btn btn-primary">{{ _lang('Update') }}</button>
							</div>
						</div>	
					</div>	
				</form>
			</div>
		</div>
	</div>
</div>

@endsection


