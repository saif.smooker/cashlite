@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
		    <div class="card-header">
				<span class="panel-title">{{ _lang('View Customer') }}</span>
			</div>
			
			<div class="card-body">
			    <table class="table table-bordered">
				    <tr><td>{{ _lang('Name') }}</td><td>{{ $customer->name }}</td></tr>
					<tr><td>{{ _lang('Company Name') }}</td><td>{{ $customer->company_name }}</td></tr>
					<tr><td>{{ _lang('Email') }}</td><td>{{ $customer->email }}</td></tr>
					<tr><td>{{ _lang('Phone') }}</td><td>{{ $customer->phone }}</td></tr>
					<tr><td>{{ _lang('Country') }}</td><td>{{ $customer->country }}</td></tr>
					<tr><td>{{ _lang('City') }}</td><td>{{ $customer->city }}</td></tr>
					<tr><td>{{ _lang('State') }}</td><td>{{ $customer->state }}</td></tr>
					<tr><td>{{ _lang('Zip') }}</td><td>{{ $customer->zip }}</td></tr>
					<tr><td>{{ _lang('Address') }}</td><td>{{ $customer->address }}</td></tr>
					<tr><td>{{ _lang('Note') }}</td><td>{{ $customer->note }}</td></tr>
			    </table>
			</div>
	    </div>
	</div>
</div>
@endsection


