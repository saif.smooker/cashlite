<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['install']], function () {
	
	Route::get('/', function(){
		return redirect('login');
	});
	
    Auth::routes(['register' => false]);
	Route::get('/logout', 'Auth\LoginController@logout');
	
	Route::group(['middleware' => ['auth','verified']], function () {
		
		Route::get('dashboard', 'DashboardController@index');
		
		//Profile Controller
		Route::get('profile/edit', 'ProfileController@edit');
		Route::post('profile/update', 'ProfileController@update');
		Route::get('profile/change_password', 'ProfileController@change_password');
		Route::post('profile/update_password', 'ProfileController@update_password');
		

		/** Admin Only Route **/
		Route::group(['middleware' => ['admin']], function () {
			
			//User Management
			Route::resource('users','UserController');

			//User Roles
			Route::resource('roles','RoleController');
			
			//Permission Controller
		    Route::get('permission/control/{user_id?}', 'PermissionController@index')->name('permission.index');
			Route::post('permission/store', 'PermissionController@store')->name('permission.store');

			//Language Controller
			Route::resource('languages','LanguageController');	
			
			//Utility Controller
			Route::match(['get', 'post'],'administration/general_settings/{store?}', 'UtilityController@settings')->name('settings.update_settings');
			Route::post('administration/upload_logo', 'UtilityController@upload_logo')->name('settings.uplaod_logo');
			Route::get('administration/database_backup_list', 'UtilityController@database_backup_list')->name('database_backups.list');
			Route::get('administration/create_database_backup', 'UtilityController@create_database_backup')->name('database_backups.create');
			Route::delete('administration/destroy_database_backup/{id}', 'UtilityController@destroy_database_backup');
			Route::get('administration/download_database_backup/{id}', 'UtilityController@download_database_backup')->name('database_backups.download');
			Route::post('administration/remove_cache', 'UtilityController@remove_cache')->name('settings.remove_cache');

			//Email Template
			/*Route::resource('email_templates','EmailTemplateController')->only([
				'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
			]);*/	
			
		});

        /** Dynamic Permission **/
		Route::group(['middleware' => ['permission']], function () {
			
			//Customer Controller
			Route::resource('customers','CustomerController');

			//Vendor Controller
			Route::resource('vendors','VendorController');

			//Account Controller
			Route::resource('accounts','AccountController');

			//Income Controller
			Route::get('income/get_table_data','IncomeController@get_table_data');
			Route::resource('income','IncomeController');

			//Expense Controller
			Route::get('expense/get_table_data','ExpenseController@get_table_data');
			Route::resource('expense','ExpenseController');

			//Currency Controller
			Route::resource('currency','CurrencyController');

			//Income & Expense Category
			Route::resource('transaction_categories','CategoryController');

			//Transfer Controller
			Route::resource('transfers','TransferController');

			//Payment Methods
			Route::resource('payment_methods','PaymentMethodController');

			//Reports Controller
			Route::match(['get','post'],'reports/income_summary','ReportController@income_summary')->name('reports.income_summary');
			Route::match(['get','post'],'reports/expense_summary','ReportController@expense_summary')->name('reports.expense_summary');

			Route::match(['get','post'],'reports/income_expense_summary','ReportController@income_expense_summary')->name('reports.income_expense_summary');

		});
	
	});

});

//Socila Login
Route::get('/login/{provider}', 'Auth\SocialController@redirect');
Route::get('/login/{provider}/callback','Auth\SocialController@callback');

//JSON data for dashboard chart
Route::get('dashboard/json_month_wise_income_expense','DashboardController@json_month_wise_income_expense')->middleware('auth');
Route::get('dashboard/json_income_by_category','DashboardController@json_income_by_category')->middleware('auth');
Route::get('dashboard/json_expense_by_category','DashboardController@json_expense_by_category')->middleware('auth');

//Ajax Select2 Controller
Route::get('ajax/get_table_data','Select2Controller@get_table_data');

Route::get('/installation', 'Install\InstallController@index');
Route::get('install/database', 'Install\InstallController@database');
Route::post('install/process_install', 'Install\InstallController@process_install');
Route::get('install/create_user', 'Install\InstallController@create_user');
Route::post('install/store_user', 'Install\InstallController@store_user');
Route::get('install/system_settings', 'Install\InstallController@system_settings');
Route::post('install/finish', 'Install\InstallController@final_touch');

//Update System
Route::get('migration/update', 'Install\UpdateController@update_migration');