<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transfers';


	public function expense()
    {
        return $this->belongsTo('App\Transaction',"expense_transaction_id")->withDefault();
    }
	
	public function income()
    {
        return $this->belongsTo('App\Transaction',"income_transaction_id")->withDefault();
    }
}