<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Transaction extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transactions';
	
	public function account()
    {
        return $this->belongsTo('App\Account')->withDefault();
    }
	
	public function income_type()
    {
        return $this->belongsTo('App\Category',"category_id")->withDefault();
    }
	
	public function expense_type()
    {
        return $this->belongsTo('App\Category',"category_id")->withDefault();
    }
	
	public function customer()
    {
        return $this->belongsTo('App\Customer',"customer_id")->withDefault();
    }
	
	public function vendor()
    {
        return $this->belongsTo('App\Vendor',"vendor_id")->withDefault();
    }
	
	public function payment_method()
    {
        return $this->belongsTo('App\PaymentMethod',"payment_method_id")->withDefault();
    }

    protected static function booted()
    {
        static::creating(function ($transaction) {
            $transaction->created_user_id = Auth::id();
        });

        static::updating(function ($transaction) {
            $transaction->updated_user_id = Auth::id();
        });
    }
	
    public function getPaidAtAttribute()
    {
        $date_format = get_date_format();
        return \Carbon\Carbon::parse($this->trans_date)->format("$date_format");
    }
}