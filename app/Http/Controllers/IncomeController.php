<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use Validator;
use Illuminate\Validation\Rule;
use DataTables;

class IncomeController extends Controller
{
	
	public function __construct()
    {
		date_default_timezone_set(get_option('timezone','Asia/Dhaka'));	
	}	

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.income.list');
	}
	
	public function get_table_data(){
		
		$currency = currency();

		$transactions = Transaction::with("account")
									->with("income_type")
									->select('transactions.*')
									->where("transactions.dr_cr","cr")
									->orderBy("transactions.id","desc");

		return Datatables::eloquent($transactions)
						->editColumn('amount', function ($trans) {
							$currency = $trans->account->currency->name;	
							return "<span class='float-right'>".decimalPlace($trans->amount, $currency)."</span>";
						})
						->editColumn('trans_date', function ($trans) {
							return $trans->paid_at;
						})
						->addColumn('action', function ($trans) {
							if($trans->category_id != 1){
								return '<form action="'.action('IncomeController@destroy', $trans['id']).'" class="text-center" method="post">'
								.'<a href="'.action('IncomeController@edit', $trans['id']).'" data-title="'._lang('Update Income') .'" class="btn btn-warning btn-xs ajax-modal"><i class="ti-pencil"></i></a>&nbsp;'
								.'<a href="'.action('IncomeController@show', $trans['id']).'" data-title="'._lang('View Income Details') .'" class="btn btn-primary btn-xs ajax-modal"><i class="ti-eye"></i></a>&nbsp;'
								.csrf_field()
								.'<input name="_method" type="hidden" value="DELETE">'
								.'<button class="btn btn-danger btn-xs btn-remove" type="submit"><i class="ti-eraser"></i></button>'
								.'</form>';
							}else{
								return '<div class="text-center"><a href="'.action('IncomeController@show', $trans['id']).'" data-title="'._lang('View Details') .'" class="btn btn-primary btn-xs ajax-modal"><i class="ti-eye"></i></a></div>';

							}
						})
						->setRowId(function ($trans) {
							return "row_".$trans->id;
						})
						->rawColumns(['amount','base_amount','status','action'])
						->make(true);							    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
		if( ! $request->ajax()){
		   return view('backend.income.create');
		}else{
           return view('backend.income.modal.create');
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	
		$validator = Validator::make($request->all(), [
			'trans_date' => 'required',
			'account_id' => 'required',
			'category_id' => 'required',
			'amount' => 'required|numeric',
			'payment_method_id' => 'required',
			'reference' => 'nullable|max:50',
			'attachment' => 'nullable|mimes:jpeg,png,jpg,doc,pdf,docx,zip',
		]);
		
		if ($validator->fails()) {
			if($request->ajax()){ 
			    return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
			}else{
				return redirect('income/create')
							->withErrors($validator)
							->withInput();
			}			
		}

		$attachment = '';
		if($request->hasfile('attachment')){
		   $file = $request->file('attachment');
		   $attachment = time().$file->getClientOriginalName();
		   $file->move(public_path()."/uploads/transactions/", $attachment);
		}

		
        $transaction = new Transaction();
	    $transaction->trans_date = $request->input('trans_date');
		$transaction->account_id = $request->input('account_id');
		$transaction->category_id = $request->input('category_id');
		$transaction->type = 'income';
		$transaction->dr_cr = 'cr';
		$transaction->amount = $request->input('amount');
		$transaction->currency_rate = $transaction->account->currency->exchange_rate;
		$transaction->customer_id = $request->input('customer_id');
		$transaction->payment_method_id = $request->input('payment_method_id');
		$transaction->reference = $request->input('reference');
		$transaction->note = $request->input('note');
		$transaction->attachment = $attachment;
		
        $transaction->save();
	
        
		if(! $request->ajax()){
           return redirect()->route('income.index')->with('success', _lang('Saved Successfully'));
        }else{
		   return response()->json(['result'=>'success', 'message'=>_lang('Saved Successfully'),'data'=>$transaction, 'table' => '#income-table']);
		}
        
   }
	

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $transaction = Transaction::find($id);
		if(! $request->ajax()){
		    return view('backend.income.view',compact('transaction','id'));
		}else{
			return view('backend.income.modal.view',compact('transaction','id'));
		} 
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $transaction = Transaction::where('id',$id)
						          ->where('category_id','!=',1)
						          ->first();
		if(! $request->ajax()){
		   return view('backend.income.edit',compact('transaction','id'));
		}else{
           return view('backend.income.modal.edit',compact('transaction','id'));
		}  
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$validator = Validator::make($request->all(), [
			'trans_date' => 'required',
			'account_id' => 'required',
			'category_id' => 'required',
			'amount' => 'required|numeric',
			'payment_method_id' => 'required',
			'reference' => 'nullable|max:50',
			'attachment' => 'nullable|mimes:jpeg,png,jpg,doc,pdf,docx,zip',
		]);
		
		if ($validator->fails()) {
			if($request->ajax()){ 
			    return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
			}else{
				return redirect()->route('income.edit', $id)
							->withErrors($validator)
							->withInput();
			}			
		}

		$attachment = "";
        if($request->hasfile('attachment'))
		{
		  $file = $request->file('attachment');
		  $attachment = time().$file->getClientOriginalName();
		  $file->move(public_path()."/uploads/transactions/", $attachment);
		}
		

		$transaction = Transaction::where('id',$id)
						          ->where('category_id','!=',1)
						          ->first();
		$previous_amount = $transaction->amount;
		$transaction->trans_date = $request->input('trans_date');
		$transaction->account_id = $request->input('account_id');
		$transaction->category_id = $request->input('category_id');
		$transaction->type = 'income';
		$transaction->dr_cr = 'cr';
		$transaction->amount = $request->input('amount');
		$transaction->customer_id = $request->input('customer_id');
		$transaction->payment_method_id = $request->input('payment_method_id');
		$transaction->reference = $request->input('reference');
		$transaction->note = $request->input('note');
		if($request->hasfile('attachment')){
			$transaction->attachment = $attachment;
		}

		$transaction->save();
	     
		
		if(! $request->ajax()){
           return redirect()->route('income.index')->with('success', _lang('Updated Successfully'));
        }else{
		   return response()->json(['result'=>'success', 'message'=>_lang('Updated Successfully'),'data'=>$transaction, 'table' => '#income-table']);
		}
	    
    }
	
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$transaction = Transaction::find($id);
		if($transaction->category_id != 1){
			$transaction->delete();
        	return redirect()->route('expense.index')->with('success',_lang('Removed Successfully'));
		}
    }
}
