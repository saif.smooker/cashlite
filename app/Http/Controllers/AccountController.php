<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use Validator;

class AccountController extends Controller
{
	
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        date_default_timezone_set(get_option('timezone','Asia/Dhaka'));
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Account::all()->sortByDesc("id");
        return view('backend.account.list',compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( ! $request->ajax()){
           return view('backend.account.create');
        }else{
           return view('backend.account.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	
        $validator = Validator::make($request->all(), [
            'name' => 'required',
			'currency_id' => 'required',
			'openning_balance' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return redirect()->route('accounts.create')
                	             ->withErrors($validator)
                	             ->withInput();
            }			
        }
	
        
        $account = new Account();
        $account->name = $request->input('name');
		$account->account_no = $request->input('account_no');
		$account->currency_id = $request->input('currency_id');
		$account->openning_balance = $request->input('openning_balance');
		$account->contact_person = $request->input('contact_person');
		$account->contact_email = $request->input('contact_email');
		$account->note = $request->input('note');

        $account->save();
        $account->currency_id = $account->currency->name;
        $account->openning_balance = decimalPlace($account->openning_balance, $account->currency->name);

        if(! $request->ajax()){
           return redirect()->route('accounts.create')->with('success', _lang('Saved Successfully'));
        }else{
           return response()->json(['result'=>'success','action'=>'store','message'=>_lang('Saved Successfully'),'data'=>$account, 'table' => '#accounts_table']);
        }
        
   }
	

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $account = Account::find($id);
        if(! $request->ajax()){
            return view('backend.account.view',compact('account','id'));
        }else{
            return view('backend.account.modal.view',compact('account','id'));
        } 
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $account = Account::find($id);
        if(! $request->ajax()){
            return view('backend.account.edit',compact('account','id'));
        }else{
            return view('backend.account.modal.edit',compact('account','id'));
        }  
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'currency_id' => 'required',
			'openning_balance' => 'required|numeric',
		]);

		if ($validator->fails()) {
			if($request->ajax()){ 
				return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
			}else{
				return redirect()->route('accounts.edit', $id)
							->withErrors($validator)
							->withInput();
			}			
		}
	
        	
		
        $account = Account::find($id);
		$account->name = $request->input('name');
		$account->account_no = $request->input('account_no');
		$account->currency_id = $request->input('currency_id');
		$account->openning_balance = $request->input('openning_balance');
		$account->contact_person = $request->input('contact_person');
		$account->contact_email = $request->input('contact_email');
		$account->note = $request->input('note');
	
        $account->save();
        $account->currency_id = $account->currency->name;
        $account->openning_balance = decimalPlace($account->openning_balance, $account->currency->name);
		
		if(! $request->ajax()){
           return redirect()->route('accounts.index')->with('success', _lang('Updated Successfully'));
        }else{
		   return response()->json(['result'=>'success','action'=>'update', 'message'=>_lang('Updated Successfully'),'data'=>$account, 'table' => '#accounts_table']);
		}
	    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = Account::find($id);
        $account->delete();
        return redirect()->route('accounts.index')->with('success',_lang('Deleted Successfully'));
    }
}