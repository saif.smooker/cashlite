<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Vendor;
use Validator;

class VendorController extends Controller
{
	
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        date_default_timezone_set(get_option('timezone','Asia/Dhaka'));
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendors = Vendor::all()->sortByDesc("id");
        return view('backend.vendor.list',compact('vendors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( ! $request->ajax()){
           return view('backend.vendor.create');
        }else{
           return view('backend.vendor.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	
        $validator = Validator::make($request->all(), [
            'name' => 'required',
			'email' => 'required|email|unique:vendors|max:255',
        ]);

        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return redirect()->route('vendors.create')
                	             ->withErrors($validator)
                	             ->withInput();
            }			
        }
	
        
        $vendor = new Vendor();
        $vendor->name = $request->input('name');
		$vendor->company_name = $request->input('company_name');
		$vendor->email = $request->input('email');
		$vendor->registration_no = $request->input('registration_no');
		$vendor->vat_id = $request->input('vat_id');
		$vendor->phone = $request->input('phone');
		$vendor->country = $request->input('country');
		$vendor->city = $request->input('city');
		$vendor->state = $request->input('state');
		$vendor->zip = $request->input('zip');
		$vendor->address = $request->input('address');
		$vendor->note = $request->input('note');

        $vendor->save();

        if(! $request->ajax()){
           return redirect()->route('vendors.create')->with('success', _lang('Saved Successfully'));
        }else{
           return response()->json(['result'=>'success','action'=>'store','message'=>_lang('Saved Successfully'),'data'=>$vendor, 'table' => '#vendors_table']);
        }
        
   }
	

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $vendor = Vendor::find($id);
        if(! $request->ajax()){
            return view('backend.vendor.view',compact('vendor','id'));
        }else{
            return view('backend.vendor.modal.view',compact('vendor','id'));
        } 
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $vendor = Vendor::find($id);
        if(! $request->ajax()){
            return view('backend.vendor.edit',compact('vendor','id'));
        }else{
            return view('backend.vendor.modal.edit',compact('vendor','id'));
        }  
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'email' => [
                'required',
                'email',
                Rule::unique('customers')->ignore($id),
            ],
		]);

		if ($validator->fails()) {
			if($request->ajax()){ 
				return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
			}else{
				return redirect()->route('vendors.edit', $id)
							->withErrors($validator)
							->withInput();
			}			
		}
	    	
		
        $vendor = Vendor::find($id);
		$vendor->name = $request->input('name');
		$vendor->company_name = $request->input('company_name');
		$vendor->email = $request->input('email');
		$vendor->registration_no = $request->input('registration_no');
		$vendor->vat_id = $request->input('vat_id');
		$vendor->phone = $request->input('phone');
		$vendor->country = $request->input('country');
		$vendor->city = $request->input('city');
		$vendor->state = $request->input('state');
		$vendor->zip = $request->input('zip');
		$vendor->address = $request->input('address');
		$vendor->note = $request->input('note');
	
        $vendor->save();
		
		if(! $request->ajax()){
           return redirect()->route('vendors.index')->with('success', _lang('Updated Successfully'));
        }else{
		   return response()->json(['result'=>'success','action'=>'update', 'message'=>_lang('Updated Successfully'),'data'=>$vendor, 'table' => '#vendors_table']);
		}
	    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = Vendor::find($id);
        $vendor->delete();
        return redirect()->route('vendors.index')->with('success',_lang('Deleted Successfully'));
    }
}